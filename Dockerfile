FROM node:10-alpine

ENV appDir /var/www/app/current
ENV AMQP_ADDRESS localhost

RUN apk update \
  && apk add python make g++ git

RUN mkdir -p /var/www/app/current
WORKDIR ${appDir}

ADD package.json ./
RUN npm i && npm i -g pm2

ADD . /var/www/app/current

EXPOSE 4500

RUN cd /var/www/app/current && npm run build

CMD ["pm2", "start", "processes.json", "--no-daemon", "--node-args", "\"AMQP_ADDRESS=${AMQP_ADDRESS}\""]
